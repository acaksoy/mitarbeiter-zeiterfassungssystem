from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, BooleanField, SelectField, DateField
from wtforms.validators import DataRequired, Optional
import re


class MitarbeiterForm(FlaskForm):
    class PersonDaten(FlaskForm):
        isPSB = BooleanField('Personalsachbearbeiter?')
        mitarbeiterID = StringField('Mitarbeiter ID')
        pw = StringField('Passwort')
        name = StringField('Vorname', validators=[DataRequired()])
        nachName = StringField('Nachname', validators=[DataRequired()])
        abteilung = SelectField('Abteilung', choices=[('#IT', 'Informatik'), ('#FI', 'Finanz'), ('#VW', 'Verwaltung')])
        gebdat = StringField('Geburtsdatum', validators=[DataRequired()])
    class AdressForm(FlaskForm):
        straße = StringField('Straße', validators=[DataRequired()])
        ort = StringField('Ort', validators=[DataRequired()])
        PLZ = IntegerField('PLZ', validators=[DataRequired()])
    class KontaktForm(FlaskForm):
        tel = IntegerField('Festnetz', validators=[DataRequired()])
        mob = IntegerField('Mobil', validators=[Optional()])
        mail = StringField('E-Mail', validators=[DataRequired()])
    class OrgaForm(FlaskForm):
        Wochenstunden = IntegerField('Wochenstunden', validators=[Optional()])
        Eintritt = StringField('Eintrittsdatum', validators=[DataRequired()])
        Probez = IntegerField('Probezeit', validators=[Optional()])



class ShowMitarbeiterForm(FlaskForm):
    abteilung = SelectField('Abteilung', choices=[('#IT', 'Informatik'), ('#FI', 'Finanz'), ('#VW', 'Verwaltung')], validators=[Optional()])

    def validate(self):
        check_validate = super(ShowMitarbeiterForm, self).validate()

        if not check_validate:
            return False

        return True


class ShowAdressForm(FlaskForm):
    straße = StringField('Straße', validators=[Optional()])
    ort = StringField('Ort', validators=[Optional()])
    PLZ = IntegerField('PLZ', validators=[Optional()])

    def validate(self):
        check_validate = super(ShowAdressForm, self).validate()

        if not check_validate:
            return False

        return True

class ShowKontaktForm(FlaskForm):
    tel = IntegerField('Festnetz', validators=[Optional()])
    mob = IntegerField('Mobil', validators=[Optional()])
    mail = StringField('E-Mail', validators=[Optional()])

    def validate(self):
        check_validate = super(ShowKontaktForm, self).validate()

        if not check_validate:
            return False

        return True

class ShowOrgaForm(FlaskForm):
    Wochenstunden = IntegerField('Wochenstunden', validators=[Optional()])
    Probez = IntegerField('Probezeit', validators=[Optional()])

    def validate(self):
        check_validate = super(ShowOrgaForm, self).validate()

        if not check_validate:
            return False

        return True

class FehltagForm(FlaskForm):
    vonDate = DateField('von', validators=[DataRequired()])
    bisDate = DateField('bis', validators=[DataRequired()])
    fehlgrund = SelectField('Grund des Fehlens', choices=['Urlaub', 'Krank', 'Zeitausgleich', 'Home Office'], validators=[DataRequired()])
    def datecheck(self):
        matchvonDate = re.match("[0-9][0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9]", str(self.vonDate.data))
        matchbisDate = re.match("[0-9][0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9]", str(self.bisDate.data))
        is_match1 = bool(matchvonDate)
        is_match2 = bool(matchbisDate)
        if is_match1 == True and is_match2 == True :
            return True
        return False




