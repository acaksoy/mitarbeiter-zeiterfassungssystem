from flask import Blueprint, render_template, request, redirect, url_for, flash, session,render_template_string, Markup
from flask_login import login_required, current_user
from werkzeug.security import generate_password_hash
import datetime, re

from .forms import ShowOrgaForm, ShowKontaktForm, ShowAdressForm, ShowMitarbeiterForm
from app.main.mdb import Arbeitstage, Stempel
from app.main.views import login_required, restricted
from .forms import MitarbeiterForm, FehltagForm
from app.main.mdb import Mitarbeiter, Adresse, Kontakt, Arbeitstage
from app.main.views import login_required, restricted



from .forms import MitarbeiterForm
from app.main.mdb import Mitarbeiter, Adresse, Kontakt, Orga, db
import flask_mongoengine

# define the blueprint
mitarbeiterliste = Blueprint('mitarbeiterliste', __name__, template_folder='pages', static_folder='static')
adress = Blueprint('adress', __name__, template_folder='pages', static_folder='static')
kontak = Blueprint('kontakt', __name__, template_folder='pages', static_folder='static')
orga = Blueprint('sonstiges', __name__, template_folder='pages', static_folder='static')
# all routes
@mitarbeiterliste.route('/')
@login_required
@restricted
def home():
    mitarbeiterliste = Mitarbeiter.objects.all()
    adress = Adresse.objects.all()
    kontak = Kontakt.objects.all()
    orga = Orga.objects.all()
    mitarbeiterEvents = Arbeitstage.objects.all()
    return render_template('list_real.html', mitarbeiterliste=mitarbeiterliste, mitarbeiterEvents=mitarbeiterEvents, adress=adress, kontak=kontak, orgas=orga, info=session)

@mitarbeiterliste.route('/add', methods=['GET', 'POST'])
@login_required
@restricted
def add():
    form = MitarbeiterForm.PersonDaten(request.form)
    mform = MitarbeiterForm.AdressForm(request.form)
    kform = MitarbeiterForm.KontaktForm(request.form)
    oform = MitarbeiterForm.OrgaForm(request.form)
    if form.validate_on_submit() and mform.validate_on_submit() and kform.validate_on_submit() and oform.validate_on_submit():
        mitarbeiterliste = Mitarbeiter(name=form.name.data,
                                      nachName=form.nachName.data,
                                      isPSB=form.isPSB.data,
                                      abteilung=form.abteilung.data,
                                           gebdat=form.gebdat.data)
        mitarbeiterliste.mitarbeiterID= mitarbeiterliste.createbenutzername()
        mitarbeiterliste.password = generate_password_hash("newuser"+str(mitarbeiterliste.gebdat))
        adress = Adresse(ort=mform.ort.data,
                             straße=mform.straße.data,
                             PLZ=mform.PLZ.data)


        kontak = Kontakt(tel=kform.tel.data,
                             mob=kform.mob.data,
                             mail=kform.mail.data)

        orga = Orga(Wochenstunden=oform.Wochenstunden.data,
                                Eintritt=oform.Eintritt.data,
                                Probez=oform.Probez.data)
        mitarbeiterliste.save()
        adress.mitarbeiterRef = mitarbeiterliste
        adress.save()
        kontak.mitarbeiterRef = mitarbeiterliste
        kontak.save()
        orga.mitarbeiterRef = mitarbeiterliste
        orga.save()

        flash("mitarbeiter added successfully.", "success")
        return redirect(url_for('.home'))
    return render_template('form_real.html', form=form, mform=mform, kform=kform, oform=oform, info=session)

#mID entspricht einer Mitarbeiter ID
#in stammdatenansicht werden Objekte von Mitarbeiter nach ID angefragt und in mitarbeiterliste gespeichert
#in adress und kontak werden Objekte angefragt, die mitarbeiterliste referenzieren

@mitarbeiterliste.route('/deactivate/<mID>', methods=['GET','POST'])
@login_required
@restricted
def deactivate(mID):
    mitarbeiterliste = Mitarbeiter.objects.get_or_404(mitarbeiterID=mID)
    if (current_user.mitarbeiterID != mitarbeiterliste.mitarbeiterID):
         Mitarbeiter.objects(mitarbeiterID=mID).update_one(set__locked=True)
         flash("Mitarbeiter deaktiviert!", "warning")
         return redirect(url_for('.home'))
    flash("Sie können sich nicht selbst deaktivieren!", "warning")
    return redirect(url_for('.home'))

@mitarbeiterliste.route('/activate/<mID>', methods=['GET','POST'])
@login_required
@restricted
def activate(mID):

    Mitarbeiter.objects(mitarbeiterID=mID).update_one(set__locked=False)
    flash("Mitarbeiter aktiviert!", "success")
    return redirect(url_for('.home'))



@mitarbeiterliste.route('/delete/<string:mID>', methods=['GET','POST'])
@login_required
@restricted
def delete(mID):
    # finde mitarbeiter mit der mID als mitarbeiterID und seine referenzen
    mitarbeiterliste = Mitarbeiter.objects.get_or_404(mitarbeiterID=mID)
    if (current_user.mitarbeiterID != mitarbeiterliste.mitarbeiterID):
        Adresse.objects(mitarbeiterRef=mID).delete()
        Kontakt.objects(mitarbeiterRef=mID).delete()
        Orga.objects(mitarbeiterRef=mID).delete()
        Stempel.objects(mitarbeiterRef=mID).delete()
        Arbeitstage.objects(mitarbeiterRef=mID).delete()
        mitarbeiterliste.delete()
        flash("Mitarbeiter entfernt!", "warning")
        return redirect(url_for('.home'))
    else:
        flash("Sie können sich nicht löschen!", "warning")
        return redirect(url_for('.home'))


@mitarbeiterliste.route('/stammdatenansicht/<string:mID>', methods=['GET', 'POST'])
@login_required
@restricted
def stammdatenansicht(mID):
    # finde mitarbeiter mit der mID als mitarbeiterID und seine referenzen
    mitarbeiterliste = Mitarbeiter.objects.get_or_404(mitarbeiterID=mID)
    adress = Adresse.objects.get_or_404(mitarbeiterRef=mID)
    kontak = Kontakt.objects.get_or_404(mitarbeiterRef=mID)
    orga = Orga.objects.get_or_404(mitarbeiterRef=mID)
    return render_template('stammdatenansicht.html', mitarbeiterliste=mitarbeiterliste, adress=adress, kontak=kontak,orga=orga, info=session)


#mit edit_profile wird ein Dokument überschrieben
#Wie bei stammdatenansicht Objekt angefragt
#Alternative Forms erstellt, damit nur die Felder bearbeitet werden die auch bearbeitet werden dürfen

@mitarbeiterliste.route('/stammdatenansicht/bearbeiten/<mID>', methods=['GET','POST'])
@login_required
@restricted
def edit_profile(mID):
# finde mitarbeiter mit der mID als mitarbeiterID und seine referenzen
    mitarbeiterliste = Mitarbeiter.objects.get_or_404(mitarbeiterID=mID)
    adress = Adresse.objects.get_or_404(mitarbeiterRef=mitarbeiterliste)
    kontak = Kontakt.objects.get_or_404(mitarbeiterRef=mitarbeiterliste)
    orga = Orga.objects.get_or_404(mitarbeiterRef=mitarbeiterliste)
    form = ShowMitarbeiterForm(request.form, obj=mitarbeiterliste)
    mform = ShowAdressForm(request.form, obj=adress)
    kform = ShowKontaktForm(request.form, obj=kontak)
    oform = ShowOrgaForm(request.form, obj=orga)

    form.populate_obj(mitarbeiterliste) # Alte Daten in die Formfelder einsetzen
    mform.populate_obj(adress)
    kform.populate_obj(kontak)
    oform.populate_obj(orga)

    if form.validate_on_submit() and mform.validate_on_submit() and kform.validate_on_submit() and oform.validate_on_submit():

        adress.save()
        kontak.save()
        mitarbeiterliste.save()
        orga.save()
        flash("Änderungen gespeichert", "success")
        return redirect(url_for('.home'))

    return render_template('bearbeiten.html', form=form, mform=mform, kform=kform, oform=oform, info=session)

@mitarbeiterliste.route('/zeitmanagement/<string:mID>', methods=['GET', 'POST'])
@login_required
@restricted
def zeitmanagement(mID):
    mitarbeiter = Mitarbeiter.objects.get(mitarbeiterID=mID)    #hier wird der Mitarbeiter gefunden
    mitarbeiterEvents = Arbeitstage.objects.all()               #jedes Event wird im "mitarbeieterevents" gespeichert
    stempel = Stempel.objects.all()

    form = FehltagForm(request.form)
    if request.method == 'POST':
        if form.datecheck():    # Prüft ob das Format der angegebenden Daten
            letzterTag = form.bisDate.data + datetime.timedelta(days=1)
            if form.fehlgrund.data == 'Urlaub':     #ifs werden benötigt um die farbe zum Fehltag anzupassen
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='yellow')
                vonDatestr = str(form.vonDate.data)         #Anzahl der verbleibenden Urlaubstage des Mitarbeiters werden angepasst
                bisDatestr = str(form.bisDate.data)
                d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
                d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
                delta = d1 - d0
                pullday = str(delta)
                mitarbeiter.urlaubTage = mitarbeiter.urlaubTage - int(pullday[0:1]) -1
            if form.fehlgrund.data == 'Krank':
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='red')
                vonDatestr = str(form.vonDate.data)         #Anzahl der Krankheitstage des Mitarbeiters werden angepasst
                bisDatestr = str(form.bisDate.data)
                d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
                d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
                delta = d1 - d0
                pullday = str(delta)
                mitarbeiter.krankheitsTage = mitarbeiter.krankheitsTage + int(pullday[0:1]) + 1
            if form.fehlgrund.data == 'Zeitausgleich':
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='orange')
            if form.fehlgrund.data == 'Home Office':
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='DeepSkyBlue')
            fehltagliste.mitarbeiterRef = Mitarbeiter.objects.get(mitarbeiterID=mID)
            fehltagliste.save()
            mitarbeiter.save()
            flash('Neuen Fehltag hinzugefügt!', 'success')
        else:
            flash('Daten müssen im Format: "JJJJ-MM-TT" angegeben werden!','warning')

    return render_template('zeitmanagement.html', mitarbeiter=mitarbeiter, stempel=stempel, mitarbeiterEvents=mitarbeiterEvents, info=session, form=form, cur_user=current_user)

@mitarbeiterliste.route('/zeitmanagement/<string:mID>/Fehltage_aendern',methods=['GET','POST'])
@login_required
@restricted
def Fehltage_aendern(mID):
    mitarbeiter = Mitarbeiter.objects.get(mitarbeiterID=mID)
    mitarbeiterEvents = Arbeitstage.objects.all()

    return render_template('zeit_aendern.html', mitarbeiter=mitarbeiter, mitarbeiterEvents=mitarbeiterEvents, info=session)

@mitarbeiterliste.route('/fehltag_edit/<string:fid>/<string:mID>/', methods=['GET', 'POST'])
@login_required
@restricted
def fehltag_edit(fid, mID):
    mitarbeiter = Mitarbeiter.objects.get(mitarbeiterID=mID)
    oldevent = Arbeitstage.objects.get_or_404(id=fid)
    mitarbeiterEvents = Arbeitstage.objects.all()

    form = FehltagForm(request.form)
    if request.method == 'POST':        # Code funktioniert genau wie im Zeitmanagement bzw fehltag_loeschen
        if form.datecheck():
            if oldevent.titel == "Urlaub":
                vonDatestr = str(oldevent.start)
                bisDatestr = str(oldevent.endanzeige)
                d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
                d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
                delta = d1 - d0
                pullday = str(delta)
                mitarbeiter.urlaubTage = mitarbeiter.urlaubTage + int(pullday[0:1]) + 1
            if oldevent.titel == "Krank":
                vonDatestr = str(oldevent.start)
                bisDatestr = str(oldevent.endanzeige)
                d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
                d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
                delta = d1 - d0
                pullday = str(delta)
                mitarbeiter.krankheitsTage = mitarbeiter.krankheitsTage - int(pullday[0:1]) - 1
            oldevent.delete()
            mitarbeiter.save()
            letzterTag = form.bisDate.data + datetime.timedelta(days=1)

            if form.fehlgrund.data == 'Urlaub':
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='yellow')
                vonDatestr = str(form.vonDate.data)
                bisDatestr = str(form.bisDate.data)
                d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
                d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
                delta = d1 - d0
                pullday = str(delta)
                mitarbeiter.urlaubTage = mitarbeiter.urlaubTage - int(pullday[0:1]) - 1
            if form.fehlgrund.data == 'Krank':
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='red')
                vonDatestr = str(form.vonDate.data)
                bisDatestr = str(form.bisDate.data)
                d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
                d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
                delta = d1 - d0
                pullday = str(delta)
                mitarbeiter.krankheitsTage = mitarbeiter.krankheitsTage + int(pullday[0:1]) + 1
            if form.fehlgrund.data == 'Zeitausgleich':
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='orange')
            if form.fehlgrund.data == 'Home Office':
                fehltagliste = Arbeitstage(start=form.vonDate.data,
                                           end=letzterTag,
                                           endanzeige=form.bisDate.data,
                                           titel=form.fehlgrund.data,
                                           color='DeepSkyBlue')
            fehltagliste.mitarbeiterRef = Mitarbeiter.objects.get(mitarbeiterID=mID)
            fehltagliste.save()
            mitarbeiter.save()
            flash('Fehltag geändert', 'success')

            return render_template('zeit_aendern.html', mitarbeiter=mitarbeiter, mitarbeiterEvents=mitarbeiterEvents, info=session)
        flash('Daten müssen im Format: "JJJJ-MM-TT" angegeben werden!', 'warning')
    return render_template('fehltag_edit.html', mitarbeiter=mitarbeiter, event=oldevent, info=session, form=form)


@mitarbeiterliste.route('/fehltag_loeschen/<string:fid>/<string:mID>', methods=['GET', 'POST'])
@login_required
@restricted
def fehltag_loeschen(fid, mID):
    mitarbeiter = Mitarbeiter.objects.get_or_404(mitarbeiterID=mID)
    event = Arbeitstage.objects.get_or_404(id=fid)
    mitarbeiterEvents = Arbeitstage.objects.all()
    returnstring = Markup("Fehltag wegen <b>" + str(event.titel)  + "</b> vom <b>" + str(event.start) + "</b> bis zum <b>" + str(event.endanzeige) + "</b> wurde gelöscht!")
    if event.titel == "Urlaub":
        vonDatestr = str(event.start)
        bisDatestr = str(event.endanzeige)
        d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
        d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
        delta = d1 - d0
        pullday = str(delta)
        mitarbeiter.urlaubTage = mitarbeiter.urlaubTage + int(pullday[0:1]) + 1
    if event.titel == "Krank":
        vonDatestr = str(event.start)
        bisDatestr = str(event.endanzeige)
        d0 = datetime.date(int(vonDatestr[0:4]), int(vonDatestr[5:7]), int(vonDatestr[8:10]))
        d1 = datetime.date(int(bisDatestr[0:4]), int(bisDatestr[5:7]), int(bisDatestr[8:10]))
        delta = d1 - d0
        pullday = str(delta)
        mitarbeiter.krankheitsTage = mitarbeiter.krankheitsTage - int(pullday[0:1]) - 1

    event.delete()
    mitarbeiter.save()
    flash(returnstring, "info")
    return render_template('zeit_aendern.html', mitarbeiter=mitarbeiter, mitarbeiterEvents=mitarbeiterEvents, info=session)

