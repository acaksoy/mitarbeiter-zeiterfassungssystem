from functools import wraps

from flask import Blueprint, render_template, redirect, request, url_for, session, flash
from flask_login import login_user, logout_user, current_user
from werkzeug.security import generate_password_hash
from app.main.forms import LoginForm, ActivationForm, PasswordChangeForm
from app.main.mdb import Mitarbeiter, Kontakt

main = Blueprint('main', __name__, template_folder='pages', static_folder='main-static')


# Decorator Definition: Unbefugter Zugriff soll verhindert werden
def login_required(f):
    @wraps(f)
    def function(*args, **kwargs):
        if not current_user.is_authenticated:  # Weiterleitung auf Login, falls nicht angemeldet
            return redirect(url_for('main.login'))
        if not (current_user.activated) and not (request.path == url_for('main.logout')):  # Weiterleitung auf Aktivierung, falls nicht aktiviert und  nicht Abmelden gewählt wurde
            if not request.path == url_for(
                    'main.activate'):  # Um Dauerschleife zu verhindern, da Aktivierung Login benötigt
                return redirect(url_for('main.activate'))
        if current_user.locked:                  #Falls Benutzer gesperrt wird, während er angemeldet ist wird er abgemeldet.
            logout_user()
            flash('Sie wurden von einem Personalsachbearbeiter gesperrt.', 'danger')
            return redirect(url_for(".login"))
        return f(*args, **kwargs)

    return function


# Decorator Definition: Seite für Nicht-Personalsachbearbeiter sperren.
def restricted(f):
    @wraps(f)
    def function(*args, **kwargs):
        if not current_user.isPSB:
            flash('Zugriff auf diese Seite nicht möglich.', 'danger')
            return redirect(url_for('meinzeitmanagement.home'))
        return f(*args, **kwargs)

    return function


@main.route('/')
@login_required
def home():
    return redirect(url_for("meinzeitmanagement.home"))


@main.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if current_user.is_authenticated:  # Falls User bereits angemeldet ist, kann er sich nicht nochmal anmelden
        return redirect(url_for(".home"))
    else:
        if form.validate_on_submit():
            user = Mitarbeiter.objects.get(mitarbeiterID=form.mitarbeiterID.data)
            login_user(user)
            session['mitarbeiterID'] = form.mitarbeiterID.data
            if not current_user.activated:  # Account-Aktivierung, bei erster Anmeldung
                return redirect(url_for(".activate"))
            return redirect(url_for("meinzeitmanagement.home"))
        return render_template("login.html", form=form)


@main.route("/activate", methods=["GET", "POST"])
@login_required
def activate():
    form = ActivationForm()
    if current_user.activated:  # Falls User bereits aktiviert ist
        return redirect(url_for("meinzeitmanagement.home"))

    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password_check.data, method="sha256")
        Mitarbeiter.objects(mitarbeiterID=current_user.mitarbeiterID).update_one(set__password=hashed_password)
        Mitarbeiter.objects(mitarbeiterID=current_user.mitarbeiterID).update_one(set__activated=True)
        flash('Passwort erfolgreich aktualisiert.', 'success')
        return redirect(url_for("meinzeitmanagement.home"))
    return render_template("ActivateAccount.html", form=form)


@main.route("/logout")
@login_required
def logout():
    logout_user()
    flash('Erfolgreich abgemeldet.', 'success')
    return redirect(url_for(".login"))

# Benutzer-Einstellungen
@main.route("/settings/", methods=["GET", "POST"])
@login_required
def settings():
    form = PasswordChangeForm()
    kontakt = Kontakt.objects.get(mitarbeiterRef=current_user.mitarbeiterID)
    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password_check.data, method="sha256")
        Mitarbeiter.objects(mitarbeiterID=current_user.mitarbeiterID).update_one(set__password=hashed_password)
        flash('Passwort erfolgreich aktualisiert.', 'success')
        return redirect(url_for(".settings"))
    return render_template("settings.html", form=form, user=current_user, kontakt=kontakt)
