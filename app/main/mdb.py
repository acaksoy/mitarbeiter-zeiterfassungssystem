from flask_mongoengine import MongoEngine, BaseQuerySet
from flask_login import UserMixin

db = MongoEngine()

class Tag(db.Document):
    field = db.StringField()

    meta = { 'collection': 'tags', 'queryset_class': BaseQuerySet}

class Mitarbeiter(db.Document, UserMixin):
    mitarbeiterID = db.StringField(required=True, primary_key=True)  # User-ID zum Einloggen wird beim erstellen generiert / siehe unten in funktion createbenutzername() ).
    password = db.StringField(required=True, max_length=128)                         # PW zum Einloggen wird beim mitarbeiter erstellen generiert (siehe mitarbeiterliste/view).
    activated = db.BooleanField(required=True, default=False)        # Wurde Benutzer aktiviert?
    locked = db.BooleanField(default=False)                          # Wurde Account gesperrt?
    name = db.StringField(max_length=80, required=True)
    nachName = db.StringField(max_length=80, required=True)
    isPSB = db.BooleanField()
    atWork = db.BooleanField()
    abteilung = db.StringField(max_length=80, required=True)
    gebdat = db.StringField()
    urlaubTage = db.IntField(default=30)
    krankheitsTage = db.IntField(default=0)
    def createbenutzername(self): #Erstelle einen Benutzernamen/ eine MitarbeiterID
        anzIDs=Mitarbeiter.objects.count()
        if self.isPSB:
            self.mitarbeiterID = 'PSB' + self.name[0:3].lower() + self.nachName[0:3].lower()+str(anzIDs) # Wenn Personalsachbearbeiter erzeuge Benutzername aus ('PSB'+erste drei Buchstaben von name und nachname + Anzahl der Objekte von Mitarbeiter
            return self.mitarbeiterID
        else:
            self.mitarbeiterID = 'MA' + self.name[0:3].lower() + self.nachName[0:3].lower()+str(anzIDs) # Wenn Mitarbeiter erzeuge Benutzername aus ('MA'+erste drei Buchstaben von name und nachname + Anzahl der Objekte von Mitarbeiter
            return self.mitarbeiterID
    def atWorkfunc(self):
        if self.atWork == True:   #Kontrolliert, ob der Mitarbeieter Anwesend ist.
            return "badge badge-success" #Falls er anwesend ist, dann wird die Badge grün
        else:
            return "badge badge-danger" #Falls er nicht anwesend ist, dann wird die Badge rot

    def __repr__(self):
        return '<User %r>' % self.id


class Adresse(db.Document):
    mitarbeiterRef = db.ReferenceField('Mitarbeiter')
    straße = db.StringField(max_length=80, required=True)
    PLZ = db.IntField(max_length=5, required=True)
    ort = db.StringField(max_length=80, required=True)


class Stempel(db.Document):
    mitarbeiterRef = db.ReferenceField('Mitarbeiter')
    stempelID = db.IntField(max_length=5, required=True, primarykey=True)
    status = db.StringField(max_Length=80, required=True)
    start = db.DateTimeField(required=True)


class Kontakt(db.Document):
    mitarbeiterRef = db.ReferenceField('Mitarbeiter')
    tel = db.IntField(max_length=20)
    mob = db.IntField(max_length=20)
    mail = db.StringField(max_length=254)

class Orga(db.Document):
    mitarbeiterRef = db.ReferenceField('Mitarbeiter')
    AbteilungID = db.IntField(max_length=5, primarykey=True)
    Wochenstunden = db.IntField(max_length=5)
    Eintritt = db.StringField(max_length=10, required=True)
    Probez = db.IntField(max_length=5)
    status = db.StringField(max_Lenght=80)
    start = db.DateTimeField(required=False)
    ende = db.DateTimeField(required=False)

class Arbeitstage(db.Document):
    mitarbeiterRef = db.ReferenceField('Mitarbeiter')
    titel = db.StringField()  #titel ist name des Events (z.B. Krank, Urlaub usw.)
    start = db.DateField()
    end = db.DateField()        # Wenn ein Fehltag zb. vom 1.Januar bis zum 2.Januar geht müssen diese Daten für den FullCalendar als 01. - 03. angegeben werden.                                                   #
    endanzeige = db.DateField() # Als String ausgegeben sieht dies jedoch so aus als ob der Fehltag falsch abgespeichert wurde. Zur Stringausgabe im zeit-aendern.html wird daher das DateField endanzeige genutzt. #
    color = db.StringField()

class IP_log(db.Document):                                  # Beim Absenden des LogIn-Forms wird die IP-Addresse gespeichert
    ipadr = db.StringField(primary_key=True)
    failedLoginAttempts = db.IntField(required = True)
    lastFailedAttempt = db.DateTimeField(required= False)