import flask
from flask_login import current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms import validators
from werkzeug.security import check_password_hash
from datetime import datetime, timedelta
from app.main.mdb import Mitarbeiter, IP_log

#Timeout-Überprüfung
def timeout(flaskform):
    current_IP = IP_log.objects.get(ipadr=flask.request.remote_addr)
    if (datetime.now() - current_IP.lastFailedAttempt) > timedelta(
            minutes=10):  # Fehlgeschlagene Anmeldungen nach 10 Minuten zurücksetzen
        IP_log.objects(ipadr=current_IP.ipadr).update_one(set__failedLoginAttempts=0)

    # Bei 5 Fehlgeschlagenen Anmeldungen - Timeout für 10 Minuten
    if IP_log.objects.get(
            ipadr=current_IP.ipadr).failedLoginAttempts > 4:
        if not (datetime.now() - current_IP.lastFailedAttempt) > timedelta(minutes=10):
            return False


    if IP_log.objects.get(ipadr=current_IP.ipadr).failedLoginAttempts > 1:
        if IP_log.objects.get(ipadr=current_IP.ipadr).failedLoginAttempts is 4:
            flaskform.mitarbeiterID.errors.append("Zu viele fehlgeschlagene Anmeldeversuche. Sie wurden für 10 Minuten "
                                             "gesperrt.")
        else:
            flaskform.mitarbeiterID.errors.append("Sie haben noch " + str(
                4 - current_IP.failedLoginAttempts) + " Versuch/e, bevor Sie eine Sperrung von 10 Minuten erhalten. ")
    return True


class LoginForm(FlaskForm):
    """
    The login form
    """
    mitarbeiterID = StringField(render_kw={"placeholder": "Mitarbeiter-ID"}, validators=[validators.DataRequired()])
    password = PasswordField(render_kw={"placeholder": "Passwort"}, validators=[validators.DataRequired()])

    def validate(self):
        check_validate = super(LoginForm, self).validate()

        # Speicherung der IP-Adresse des Requests, falls noch nicht vorhanden
        try:
            IP_log.objects.get(ipadr=flask.request.remote_addr)
        except:
            newIPConnection = IP_log(ipadr=flask.request.remote_addr, failedLoginAttempts=0, lastFailedAttempt=datetime.now())
            newIPConnection.save()

        # Validators
        if not check_validate:
            return False

        # Falls Benutzer sich zu oft falsch angemeldet hat (Aktuelles Limit bei 5 fehlgeschlagenen Anmeldungen,
        # 10 Minuten Sperre (s.o.))
        if not timeout(self):
            self.mitarbeiterID.errors.append(
                'Zu viele fehlgeschlagene Anmeldeversuche. Sie müssen 10 Minuten warten, bevor Sie sich wieder anmelden können.')
            return False

        # Existiert Benutzer - try, weil sonst Fehlermeldung ausgegeben wird, falls Benutzer nicht existiert. (
        # Referenz auf nicht bestehendes Objekt)
        try:
            user = Mitarbeiter.objects.get(mitarbeiterID=self.mitarbeiterID.data)
        except:
            IP_log.objects(ipadr=flask.request.remote_addr).update(inc__failedLoginAttempts=1,
                                                                   set__lastFailedAttempt=datetime.now())

            self.mitarbeiterID.errors.append(
                'Die Mitarbeiter-ID oder das Passwort ist falsch. Bitte wiederholen Sie die Eingabe.')
            return False

        # Passwortüberprüfung
        if not check_password_hash(user.password, self.password.data):
            IP_log.objects(ipadr=flask.request.remote_addr).update(inc__failedLoginAttempts=1,
                                                                   set__lastFailedAttempt=datetime.now())

            self.mitarbeiterID.errors.append(
                'Die Mitarbeiter-ID oder das Passwort ist falsch. Bitte wiederholen Sie die Eingabe.')
            return False

        # Wurde der Account gesperrt? Setzt Login-Versuche zurück, da "theoretisch" erfolgreicher Login
        if user.locked:
            IP_log.objects(ipadr=flask.request.remote_addr).update_one(set__failedLoginAttempts=0)
            self.mitarbeiterID.errors.clear()
            self.mitarbeiterID.errors.append(
                'Dieser Account wurde gesperrt. Bitte kontaktieren Sie einen zuständigen Personalsachbearbeiter.')
            return False

        # Counter Zurücksetzen beim erfolgreichen Anmelden
        IP_log.objects(ipadr=flask.request.remote_addr).update_one(set__failedLoginAttempts=0)
        return True


class ActivationForm(FlaskForm):
    password = PasswordField(render_kw={"placeholder": "Neues Passwort eingeben"},
                             validators=[validators.DataRequired()])
    password_check = PasswordField(render_kw={"placeholder": "Passwort bestätigen"},
                                   validators=[validators.DataRequired(), validators.equal_to('password',
                                                                                              'Die Passwörter stimmen nicht überein. Bitte überprüfen Sie die Eingabe.')
                                       , validators.length(1, 128, 'Maximale Länge für das Passwort überschritten.')])

    def validate(self):
        check_validate = super(ActivationForm, self).validate()
        if not check_validate:  # Passwörter stimmen nicht überein
            return False

        return True

# Passwort ändern - In Benutzer-Einstellungen - Unabhängig von AP3
class PasswordChangeForm(FlaskForm):
    old_password = PasswordField(render_kw={"placeholder": "Altes Passwort eingeben"},
                                 validators=[validators.DataRequired()])
    new_password = PasswordField(render_kw={"placeholder": "Neues Passwort eingeben"},
                                 validators=[validators.DataRequired()])
    password_check = PasswordField(render_kw={"placeholder": "Neues Passwort bestätigen"},
                                   validators=[validators.DataRequired(), validators.equal_to('new_password',
                                                                                              'Die Passwörter stimmen nicht überein.')
                                       , validators.length(1, 128, 'Maximale Länge für das Passwort überschritten.')])

    def validate(self):
        check_validate = super(PasswordChangeForm, self).validate()
        if not check_validate:  # Passwörter stimmen nicht überein
            return False

        if not check_password_hash(current_user.password, self.old_password.data):
            self.password_check.errors.append(
                'Eingegebenes Passwort ist falsch.')
            return False

        return True
