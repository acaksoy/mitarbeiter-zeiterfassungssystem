function capsLock(){    // Event-Listener, der je nach Status der Umschalt-Taste, die Klasse "text" einblendet oder ausblendet (Wird im body mit onload geladen)
document.documentElement.addEventListener('keyup',function(event) {

  if (event.getModifierState("CapsLock")) {
    text.style.display = "block";
  } else {
    text.style.display = "none";
  }
});

}

function sort(isPSB) {
  var table = document.getElementById("Stempelliste");
  str_start = document.getElementById("start_date").value;
  str_end = document.getElementById("end_date").value;

  var stateMap = new Map();
  var timeMap = new Map();
  //speichere Wert von ändern Button in safeMap
  var safeMap = new Map();
  var rows = table.rows;
  var Index = 0;

    //Gehe über jede Reihe
  for (i = 1; i < (rows.length); i++) {
    str_zeitpunkt = rows[i].cells[0].innerHTML;
    //ist der Stempelzeitunpunkt nach Startdatum und vor Enddatum, trage in Map ein
    if (Date.parse(str_zeitpunkt) >= Date.parse(start_date.value) && Date.parse(str_zeitpunkt) <= Date.parse(str_end) ) {
      status = rows[i].cells[1].innerHTML;
      stateMap.set(Index, status)
      timeMap.set(Index, str_zeitpunkt)

      if (isPSB =='PSB') {
        var aendernButton = rows[i].cells[2].childNodes;
        safeMap.set(Index, aendernButton)
       }

      Index++;
    }
  }
    // Alten tablebody löschen und neuen erstellen
    var old_tbody = table.getElementsByTagName('tbody')[0];;
    var new_tbody = document.createElement('tbody');
    for (i = 0; i < timeMap.size; i++) {
        let newRow = new_tbody.insertRow(i);
        let newCellDate = document.createElement("td");
        let newCellDateText = document.createTextNode(timeMap.get(i));
        newCellDate.appendChild(newCellDateText);
        newRow.appendChild(newCellDate);
        let newCellState = document.createElement("td");
        let newCellStateText = document.createTextNode(stateMap.get(i));
        newCellState.appendChild(newCellStateText);
        newRow.appendChild(newCellState);



        if (isPSB =='PSB') {

            var newCellButton = document.createElement("td");
            var childNodes = safeMap.get(i);
            newCellButton.append(childNodes[1]);
            newRow.appendChild(newCellButton);
        }
    }
  old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
}