from flask import Blueprint, render_template, session
from flask_login import login_required, current_user
from app.main.mdb import Arbeitstage

from app.main.mdb import Stempel

# define the blueprint
MeinZeitmanagement = Blueprint('meinzeitmanagement', __name__, template_folder='pages', static_folder='static')


# all routes
@MeinZeitmanagement.route('/', methods=['GET','POST'])
@login_required
def home():

    stempel = Stempel.objects.all()
    latest = Stempel.objects(mitarbeiterRef=current_user.id).order_by('-stempelID').first()
    mitarbeiterEvents = Arbeitstage.objects.all()  #Wir speichern alle Events(z.B. Krank, Urlaub usw.) im diesen Variable

    return render_template('list_mz.html', stempel=stempel, mitarbeiterEvents=mitarbeiterEvents, latest=latest, info=session, user=current_user)
