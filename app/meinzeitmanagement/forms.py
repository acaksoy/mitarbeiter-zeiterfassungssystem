from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired

class MitarbeiterForm(FlaskForm):
    class TestForm(FlaskForm):
        vonDate = DateField('von', validators=[DataRequired()])
        bisDate = DateField('bis', validators=[DataRequired()])
        fehlgrund = SelectField('Grund des Fehlens', choices=['Urlaub','Krank','Zeitausgleich','Home Office'], validators=[DataRequired()])
create = SubmitField('Create')