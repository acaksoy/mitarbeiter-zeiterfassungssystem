from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, DateTimeField
from wtforms.validators import DataRequired

class StempelForm(FlaskForm):

    start = DateTimeField('Zeitpunkt', format='%H:%M', validators=[DataRequired()])
    create = SubmitField('Create')
