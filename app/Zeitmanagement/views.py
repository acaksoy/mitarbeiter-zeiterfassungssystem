from flask import Blueprint, render_template, request, redirect, url_for, flash, session
from flask_login import current_user
from app.main.mdb import Stempel
from datetime import *
from .forms import StempelForm
from app.main.views import login_required, restricted
# define the blueprint
stempel = Blueprint('stempel', __name__, template_folder='pages')

# all routes

@stempel.route('/stempeln/')
@login_required
def stempeln():
    anzahl = Stempel.objects.count()
    if current_user.atWork == True:
        status = "ausstempeln"
    else:
        status = "einstempeln"

    if status == 'einstempeln':
        current_user.atWork = True
    else:
        current_user.atWork = False

    current_user.save()
    stempel = Stempel(stempelID=anzahl, mitarbeiterRef=current_user.id, start=datetime.now().strftime('%Y-%m-%d %H:%M'), status=status)
    stempel.save()

    return redirect(url_for('meinzeitmanagement.home'))

#Diese Funktion wird nicht verwendet, um keine Unstimmigkeiten hervorzurufen wird sie jedoch nicht gelöscht (Bereits vorhandene Test von anderen AP´s sollen nicht nochmal angepasst werden)
@stempel.route('/aendern/<int:sid>', methods=['GET', 'POST'])
@login_required
@restricted
def aendern(sid):
    form = StempelForm(request.form)
    stempel=Stempel.objects.get_or_404(stempelID=sid)
    if form.validate():
        date = stempel.start.date()
        stempel.status = form.status.data
        stempel.start = datetime.combine(date, form.start.data.time())
        stempel.save()
        return redirect(url_for('meinzeitmanagement.home'))
    return render_template('form_stempel.html', form=form, info=session)


@stempel.route('/psb_aendern/<int:sid>/<string:mid>', methods=['GET', 'POST'])
@login_required
@restricted
def psb_aendern(sid, mid):
    form = StempelForm(request.form)
    stempel=Stempel.objects.get_or_404(stempelID=sid)
    if form.validate():
        datum = stempel.start.date()
        stempel.start = datetime.combine(datum, form.start.data.time())
        stempel.save()
        return redirect(url_for('mitarbeiterliste.zeitmanagement', mID=mid, stempel=stempel))
    return render_template('form_stempel.html', form=form, info=session)




