from datetime import timedelta
from flask import Flask, session, g
from flask_assets import Environment
from flask_mongoengine import MongoEngine, MongoEngineSessionInterface
from flask_login import LoginManager, current_user
from flask_debugtoolbar import DebugToolbarExtension

from webassets.loaders import PythonLoader as PythonAssetsLoader

from app import assets
from app.main.mdb import Mitarbeiter
from app.mitarbeiterliste.views import kontak, orga

assets_env = Environment()


def create_app(config=None):
    """
    Factory pattern; create new app with specified config
    :param config: configuration object
    :return: app object
    """

    # new app object
    app = Flask('flask-example')

    # configure app
    app.config.from_object(config)

    # set up database
    db = MongoEngine()
    db.init_app(app);

    # use mongo engine for session store
    app.session_interface = MongoEngineSessionInterface(db)

    # set up assets
    assets_env.init_app(app)

    # set up toolbar
    debug_toolbar = DebugToolbarExtension()
    debug_toolbar.init_app(app)

    # initialize login manager
    login_manager = LoginManager()
    login_manager.login_view = "main.login"
    login_manager.login_message_category = "warning"
    login_manager.init_app(app)
    login_manager.login_message = False

    # Timeout Regelung - Bei 20 Minuten Inaktivität, wird der Benutzer automatisch abgemeldet.
    @app.before_request
    def before_request():
        session.permanent = True
        app.permanent_session_lifetime = timedelta(minutes=20)
        session.modified = True
        g.user = current_user

    @login_manager.user_loader
    def load_user(user_id):
        return Mitarbeiter.objects.get(pk=user_id)

    # import and register the different asset bundles
    assets_loader = PythonAssetsLoader(assets)
    for name, bundle in assets_loader.load_bundles().items():
        assets_env.register(name, bundle)

    # load blueprints
    load_blueprints(app)

    return app


def load_blueprints(app):
    """
    Load all blueprints
    :param app: app in which blueprints are registered
    :return:
    """

    from .main.views import main
    from .mitarbeiterliste.views import mitarbeiterliste
    from .mitarbeiterliste.views import adress
    from .Zeitmanagement.views import stempel
    from .meinzeitmanagement.views import MeinZeitmanagement


    # register blueprints
    app.register_blueprint(main)
    app.register_blueprint(mitarbeiterliste, url_prefix='/mitarbeiterliste')
    app.register_blueprint(adress, url_prefix='/mitarbeiterliste')
    app.register_blueprint(kontak, url_prefix='/mitarbeiterliste')
    app.register_blueprint(orga, url_prefix='/mitarbeiterliste')
    app.register_blueprint(stempel, url_prefix='/stempel')
    app.register_blueprint(MeinZeitmanagement, url_prefix='/meinzeitmanagement')
