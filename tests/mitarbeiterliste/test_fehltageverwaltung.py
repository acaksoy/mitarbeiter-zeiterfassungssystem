#! ../env/bin/python
# -*- coding: utf-8 -*-
import flask
import pytest
from flask import session

create_user = True


@pytest.mark.usefixtures("testapp")
class TestAccessabilityFehltage:    # Prüfen ob Seiten geladen werden

    def test_Fehltage_aendern(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        assert rs.status_code == 200
        rs = testapp.get('/mitarbeiterliste/zeitmanagement/2/Fehltage_aendern')
        assert rs.status_code == 200

    def test_fehltag_edit(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        assert rs.status_code == 200
        rs = testapp.get('/mitarbeiterliste/fehltag_edit/1f0c820f638bbc01a576d6e1/2/')
        assert rs.status_code == 200


class TestFunctionalityFehltage:    # Prüfen der Funktionen zum Fehltage bearbeiten bei falschen und korrekten Eingaben
    def test_neuerFehltag_falscheingabe(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rs = testapp.post('/mitarbeiterliste/zeitmanagement/2', data=dict(vonDate="falsches", bisDate="Format", fehlgrund="Urlaub"), follow_redirects=True)
        assert b'im Format' in rs.data

    def test_neuerFehltag(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rs = testapp.post('/mitarbeiterliste/zeitmanagement/2', data=dict(vonDate="2020-02-02", bisDate="2020-02-02", fehlgrund="Urlaub"),follow_redirects=True)
        assert b'Neuen Fehltag' in rs.data

    def test_fehltag_edit_falscheingabe(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rs = testapp.post('/mitarbeiterliste/fehltag_edit/1f0c820f638bbc01a576d6e1/2/', data=dict(fehlgrund="Urlaub", vonDate="falsches", bisDate="2020-02-03"), follow_redirects=True)
        assert b'im Format' in rs.data

    def test_fehltag_edit(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rs = testapp.post('/mitarbeiterliste/fehltag_edit/1f0c820f638bbc01a576d6e1/2/', data=dict(fehlgrund="Urlaub", vonDate="2020-02-03", bisDate="2020-02-03"), follow_redirects=True)
        assert b'Fehltag ge' in rs.data

    def test_fehltag_loeschen(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rs = testapp.post('/mitarbeiterliste/fehltag_loeschen/2f0c820f638bbc01a576d6e1/2')
        assert b'Fehltag wegen' in rs.data