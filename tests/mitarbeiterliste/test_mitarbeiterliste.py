import pytest
from werkzeug import testapp
from app.main.mdb import Mitarbeiter

@pytest.mark.usefixtures("testapp")
class TestURLs:

    def test_mitarbeiterliste_anzeigen(self, testapp): #Mitarbeiterliste
        us1 = testapp.post('/login', data=dict(userID='psbT', password='test'), follow_redirects=True)
        assert us1.status_code == 200
        us2 = testapp.get('/mitarbeiterliste/',follow_redirects=True)
        assert  us2.status_code == 200

    def test_stammdaten_add(self, testapp):
        post1 = testapp.post('/login', data=dict(userID='psbT', password='test'), follow_redirects=True)
        assert post1.status_code == 200
        postML = testapp.get('/mitarbeiterliste/add', follow_redirects=True)
        assert postML.status_code == 200
        addtest = testapp.post('/mitarbeiterliste/add', data=dict(name = 'Versuchs', nachName = 'Ratte', isPSB = False, abteilung = 'IT', gebdat = '11.11.2011',
                                                                  ort = 'Krempten', PLZ = '99999', straße = 'test str. 3',
                                                                  tel = 9873213, mob = 9123821, mail = 'test@testadd.de',
                                                                  Wochenstunden = 33,
                                                                  Eintritt = '11.11.2011',
                                                                  Probez = 30))
        assert addtest.status_code == 200

        addtestPflichtfelder = testapp.post('/mitarbeiterliste/add',
                               data=dict(name='Versuchs', nachName='Kaninchen', isPSB=False, abteilung='IT',
                                         gebdat='11.11.2011',
                                         ort='Krempten', PLZ='99999', straße='test str. 3',
                                         tel=9873213, mail='test@ded.de',
                                         Eintritt='11.11.2011'), follow_redirects=True)
        assert addtestPflichtfelder.status_code == 200 

    def test_ansicht(self, testapp):
        testansicht = testapp.get('/mitarbeiterliste/stammdatenansicht/maT', follow_redirects=True)
        assert testansicht.status_code == 200

    def test_bearbeiten(self, testapp):
        testbearbeiten = testapp.post('/mitarbeiterliste/stammdatenansicht/bearbeiten/maT', data=dict(name='Versuchs', nachName='Kaninchen', isPSB=False, abteilung='IT',
                                         gebdat='11.11.2011',
                                         ort='Krempten', PLZ='99999', straße='test str. 3',
                                         tel=9873213, mail='test@test.de',
                                         Eintritt='11.11.2001'), follow_redirects=True)
        assert testbearbeiten.status_code == 200

    def test_delete(self,testapp):
        testdelete = testapp.post('mitarbeiterliste/delete/AnBa12', follow_redirects=True) #Lösche Mitarbeiter Anna Bauer mit ID AnBa12
        assert testdelete.status_code == 200                                               #AnBa12 muss in DB sein



