import pytest
from importlib_metadata.tests import data


@pytest.mark.usefixtures("testapp")
class TestStammdaten:
    def test_add_url_input(self, testapp):
        rs = testapp.post('/add', data=dict(name='test', nachname='tester', isPSB=False, abteilung='IF', gebdat='11.11.2011', ort='testien', straße='test str. 3',
                                            PLZ='33333', tel='0988372', mob='123123', mail='mail@mail.de', Wochenstunden='33', Eintritt='12.12.2012'
                                            ))
        assert rs

    def test_add_url(self, testapp):
        rs = testapp.post('/add')
        assert rs.status_code == 200