import flask
import pytest
from bson import ObjectId
from werkzeug.security import generate_password_hash
from app.factory import create_app
from app.main.mdb import db, Mitarbeiter, Arbeitstage, Stempel, Adresse, Kontakt, Orga, IP_log

@pytest.fixture()
def testapp(request):
    app = create_app('app.config.TestingConfig')
    testuser_not_activated = Mitarbeiter(mitarbeiterID="1", password=generate_password_hash("test"), activated=False,
                                         name="Test",
                                         nachName="User", abteilung="Test")
    testuser_not_activated.save()

    testuser_activated = Mitarbeiter(mitarbeiterID="2", password=generate_password_hash("test"), activated=True, isPSB=False,
                                     name="Test",
                                     nachName="User", abteilung="Test")
    testuser_activated.save()
    #Zurücksetzen der fehlgeschlagenen LogIn-Versuche, da sonst Tests fehlschlagen
    IP_log.objects(ipadr="127.0.0.1").update_one(set__failedLoginAttempts=0)


    testuser_stempel = Mitarbeiter(mitarbeiterID="4", password=generate_password_hash("test"), activated=True, isPSB=True,
                                   name="Test", nachName="User", abteilung="Test", atWork=True)
    testuser_stempel.save()

    teststempel = Stempel(mitarbeiterRef="4", stempelID="1", status="einstempeln", start="2020-06-27 05:05")
    teststempel.save()

    psb_test = Mitarbeiter(mitarbeiterID="psbT",password=generate_password_hash("test"), activated=True,
                                         name="PSB",
                                         nachName="User", abteilung="Test", isPSB= True)
    psb_test.save()
    psb_arTage = Arbeitstage(mitarbeiterRef= "psbT", titel='Urlaub',
                             start='2020-07-13',end='2020-07-17',
                             color='yellow')
    psb_arTage.save()
    psb_arTage2 = Arbeitstage(mitarbeiterRef="psbT", titel='Krank',
                             start='2020-06-22', end='2020-06-26',
                             color='red')
    psb_arTage2.save()

    ####################################

    ma_test = Mitarbeiter(mitarbeiterID="maT", password=generate_password_hash("test"), activated=True,
                           name="Mitarbeiter",
                           nachName="User", abteilung="Test", isPSB= False)
    ma_test.save()

    ma_stamAdr = Adresse(ort='Krempten',
                             straße='test str. 3',
                             PLZ=99999, mitarbeiterRef="maT")
    ma_stamAdr.save()

    ma_stamKont = Kontakt(tel=12312313,
                             mob=32132132,
                             mail='test@test.de', mitarbeiterRef="maT")
    ma_stamKont.save()

    ma_stamOrg = Orga(Wochenstunden = 30,
                         Eintritt = '11.11.2011',
                         Probez = 0 , mitarbeiterRef="maT")
    ma_stamOrg.save()



    ma_arTage = Arbeitstage(mitarbeiterRef="maT", titel='Home-Office',
                             start='2020-06-04', end='2020-06-06',
                             color='blue')
    ma_arTage.save()

    ma_arTage2 = Arbeitstage(mitarbeiterRef="maT", titel='Urlaub',
                            start='2020-07-13', end='2020-07-17',
                            color='yellow')
    ma_arTage2.save()

    #####################################

    mitarbeiter1 = Mitarbeiter(mitarbeiterID="MaMü11",password=generate_password_hash("test"), activated=True,
                                         name="Marcus",
                                         nachName="Müller", abteilung="#IT", isPSB= False)
    mitarbeiter1.save()

    mitarbeiter2 = Mitarbeiter(mitarbeiterID="AnBa12", password=generate_password_hash("test"), activated=True,
                               name="Anna",
                               nachName="Bauer", abteilung="#FI", isPSB= False)
    mitarbeiter2.save()

    mitarbeiter3 = Mitarbeiter(mitarbeiterID="EmSc13", password=generate_password_hash("test"), activated=True,
                               name="Emma",
                               nachName="Schmidt", abteilung="#IT", isPSB= False)
    mitarbeiter3.save()

    #######################

    Aendertag = Arbeitstage(titel="Krank", start="2020-02-02", end="2020-02-03", endanzeige="2020-02-04", id=ObjectId("1f0c820f638bbc01a576d6e1"))
    Aendertag.mitarbeiterRef = testuser_activated
    Aendertag.save()

    Loeschtag = Arbeitstage(titel="Urlaub", start="2020-02-02", end="2020-02-03", endanzeige="2020-02-02", id=ObjectId("2f0c820f638bbc01a576d6e1"))
    Loeschtag.mitarbeiterRef = testuser_activated
    Loeschtag.save()

    client = app.test_client()

    return client

