#! ../env/bin/python
# -*- coding: utf-8 -*-
import flask
import pytest
from flask import session

create_user = True


@pytest.mark.usefixtures("testapp")
class TestAccessibility:  # Hier wird überprüft, ob die Seiten überhaupt geladen werden können
    def test_login(self, testapp):
        rs = testapp.get('/login')
        assert rs.status_code == 200

    def test_logout(self, testapp):
        rs = testapp.get('/logout')
        assert rs.status_code == 302

    def test_activation(self, testapp):
        rs1 = testapp.post('/login', data=dict(mitarbeiterID='1', password='test'), follow_redirects=False)
        assert rs1.status_code == 302
        rs = testapp.get('/activate')
        assert rs.status_code == 200


class TestFunctionality:  # Hier wird überprüft, ob die Anmeldung/Abmeldung bzw. Aktivierung funktionieren

    def test_login_logout(self, testapp):
        rs1 = testapp.post('/login', data=dict(mitarbeiterID='1', password='test'), follow_redirects=False)
        assert rs1.status_code == 302
        rs2 = testapp.get('/logout', follow_redirects=True)
        assert b"Anmelden" in rs2.data

    def test_account_activation(self, testapp):
        rs1 = testapp.post('/login', data=dict(mitarbeiterID='1', password='test'), follow_redirects=True)
        assert rs1.status_code == 200
        assert b"Neues Passwort eingeben" in rs1.data
        rs2 = testapp.post('/activate', data=dict(password='test', password_check='test'), follow_redirects=False)
        assert rs2.status_code == 302


class TestLoginRequired:  # Hier wird überprüft, ob alle Seiten einen Log-In benötigen

    def test_loginrequired_meinzeitmanagement(self, testapp):
        rs1 = testapp.get('/meinzeitmanagement/')
        assert rs1.status_code == 302
        rs2 = testapp.get('/settings/')
        assert rs2.status_code == 302

    def test_loginrequired_Mitarbeiterliste(self, testapp):
        rs1 = testapp.get('/mitarbeiterliste/')
        assert rs1.status_code == 302
        rs2 = testapp.get('/mitarbeiterliste/add')
        assert rs2.status_code == 302
        rs3 = testapp.get('/mitarbeiterliste/stammdatenansicht/1')
        assert rs3.status_code == 302
        rs4 = testapp.get('/mitarbeiterliste/stammdatenansicht/bearbeiten/1')
        assert rs4.status_code == 302
        rs5 = testapp.get('/mitarbeiterliste/zeitmanagement/1')
        assert rs5.status_code == 302
        rs6 = testapp.get('/mitarbeiterliste/zeitmanagement/1/Fehltage_aendern')
        assert rs6.status_code == 302
        rs7 = testapp.get('/mitarbeiterliste/deactivate/1')
        assert rs7.status_code == 302
        rs8 = testapp.get('/mitarbeiterliste/activate/1')
        assert rs8.status_code == 302
        rs9 = testapp.get('/mitarbeiterliste/delete/1')
        assert rs9.status_code == 302

    def test_loginrequired_settings(self, testapp):
        rs1 = testapp.get('/settings/')
        assert rs1.status_code == 302

    def test_loginrequired_activation(self, testapp):
        rs1 = testapp.get('/activate')
        assert rs1.status_code == 302

    def test_loginrequired_stempeln(self, testapp):
        rs1 = testapp.get('/stempel/aendern/0')
        assert rs1.status_code == 302
        rs2 = testapp.get('/stempel/stempeln/')
        assert rs2.status_code == 302


class TestRestriction:  # Hier wird überprüft, ob alle Seiten, die nur für Personalsachbearbeiter aufrufbar sein sollen, auch wirklich gesperrt sind

    def test_restriction_Mitarbeiterliste(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='2', password='test'), follow_redirects=True)
        assert rs.status_code == 200
        rs1 = testapp.get('/mitarbeiterliste/', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs1.data
        rs2 = testapp.get('/mitarbeiterliste/add', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs2.data
        rs3 = testapp.get('/mitarbeiterliste/stammdatenansicht/1', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs3.data
        rs4 = testapp.get('/mitarbeiterliste/stammdatenansicht/bearbeiten/1', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs4.data
        rs5 = testapp.get('/mitarbeiterliste/zeitmanagement/1', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs5.data
        rs6 = testapp.get('/mitarbeiterliste/zeitmanagement/1/Fehltage_aendern', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs6.data
        rs7 = testapp.get('/mitarbeiterliste/deactivate/1', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs7.data
        rs8 = testapp.get('/mitarbeiterliste/activate/1', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs8.data
        rs9 = testapp.get('/mitarbeiterliste/delete/1', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs9.data

    def test_restriction_stempel(self, testapp):
        rs = testapp.post('/login', data=dict(mitarbeiterID='2', password='test'), follow_redirects=True)
        assert rs.status_code == 200
        rs1 = testapp.get('/stempel/aendern/0', follow_redirects=True)
        assert b'Zugriff auf diese Seite nicht m\xc3\xb6glich.' in rs1.data


class TestTimeout:

    def test_timeout(selfself, testapp):
        for x in range(0, 6):
            rs = testapp.post('/login', data=dict(mitarbeiterID='NonExistingUser', password='test'),
                              follow_redirects=True)
        assert b'Zu viele fehlgeschlagene Anmeldeversuche.' in rs.data
