#! ../env/bin/python
# -*- coding: utf-8 -*-
import pytest


@pytest.mark.usefixtures("testapp")
class TestStempel:
    def test_stempelbutton(self, testapp):
        testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rv = testapp.get('/stempel/stempeln', follow_redirects=True)
        assert rv.status_code == 200

    def test_stempelaendern(self, testapp):
        testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rv = testapp.get('/stempel/aendern/9', follow_redirects=True)
        assert rv.status_code == 200

    def test_psbstempelaendern(self, testapp):
        testapp.post('/login', data=dict(mitarbeiterID='4', password='test'), follow_redirects=True)
        rv = testapp.get('/stempel/psb_aendern/9/345')
        assert rv.status_code == 200
