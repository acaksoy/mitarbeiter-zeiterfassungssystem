import pytest
from werkzeug import testapp
from app.main.mdb import Mitarbeiter

@pytest.mark.usefixtures("testapp")
class TestURLs:
    def test_meinzeitmanagement_anzeigen(self,testapp): #Zeitmanagement des Benutzers
        us2 = testapp.get('/meinzeitmanagement',follow_redirects=True)
        assert us2.status_code == 200

    def test_zeitmanagement_mitarbeiters_anzeigen(self, testapp):  #Zeitmanagement eines Mitarbeiters
        us1 = testapp.post('/login', data=dict(userID='psbT', password='test'))
        assert us1.status_code == 200
        us3 = testapp.get('/mitarbeiterliste/zeitmanagement/maT', follow_redirects=True)
        assert us3.status_code == 200