#!/usr/bin/env python

import os, sys

from werkzeug.security import generate_password_hash
from flask_script import Manager
from flask_script.commands import ShowUrls, Clean, Command, prompt_bool
from app.factory import create_app, assets_env
from app.main.mdb import Mitarbeiter, Kontakt, IP_log, Orga, Adresse, Arbeitstage


class InitDatabase(Command):
    def __index__(self):
        super(InitDatabase, self).__init__()

    def run(self):
        verified = prompt_bool('Testbenutzer erstellen/zurücksetzten?')
        if verified:
            admin = Mitarbeiter(mitarbeiterID="admin", password=generate_password_hash("test"), activated=True, isPSB=True, name="Dominik", nachName="Molz", abteilung="#IT", gebdat="01-01-2000")
            admin.save()
            sys.stdout.write('Personalsachbearbeiter mit ID admin und Passwort test erstellt. \n')
            new_user = Mitarbeiter(mitarbeiterID="user", password=generate_password_hash("test"), activated=False, isPSB=False, name="Daniel", nachName="Heiserer", abteilung="#IT", gebdat="01-01-2000")
            new_user.save()
            sys.stdout.write('Normaler Benutzer mit ID user und Passwort test erstellt. \n')

            # Damit nicht mehrere Kontakt Objekte für Admin bzw. New_User angelegt werden
            # Try/Except, weil sonst ggf. Referenz auf nicht existierendes Objekt
            try:
                Kontakt.objects.get(mitarbeiterRef=admin)
            except:
                kontakt = Kontakt(mail="admin@test.de",tel="12345", mob="12345")
                orga = Orga(AbteilungID=1, Eintritt="01-01-2020", Wochenstunden="40", Probez="20", status="atWork", start="2020-01-01", ende="2020-01-01")
                adresse = Adresse(straße="Test-Street", PLZ=12345 , ort="Test-City")
                arbeitstage = Arbeitstage()
                kontakt.mitarbeiterRef = admin
                orga.mitarbeiterRef = admin
                adresse.mitarbeiterRef = admin
                arbeitstage.mitarbeiterRef = admin
                arbeitstage.save()
                adresse.save()
                orga.save()
                kontakt.save()
                sys.stdout.write('Es wurden Stammdaten für admin erstellt. \n')

            try:
                Kontakt.objects.get(mitarbeiterRef=new_user)
            except:
                kontakt = Kontakt(mail="user@test.de",tel="12345", mob="12345")
                orga = Orga(AbteilungID=1, Eintritt="01-01-2020", Wochenstunden="40", Probez="20")
                adresse = Adresse(straße="Test-Street", PLZ=12345 , ort="Test-City")
                arbeitstage = Arbeitstage()
                kontakt.mitarbeiterRef = new_user
                orga.mitarbeiterRef = new_user
                adresse.mitarbeiterRef = new_user
                arbeitstage.mitarbeiterRef = new_user
                arbeitstage.save()
                adresse.save()
                orga.save()
                kontakt.save()
                sys.stdout.write('Es wurden Stammdaten für user erstellt. \n')

        #Time-Outs zurücksetzen - ggf. notwendig nach Ausführung der pytests "test_login_logout"
        log_reset = prompt_bool('Sollen die Timeouts für alle IP-Adressen zurückgesetzt werden?')
        if log_reset:
            for x in IP_log.objects:
               IP_log.objects(ipadr=x.ipadr).update_one(set__failedLoginAttempts=0)
               sys.stdout.write('Timeout für IP-Addresse '+x.ipadr+' zurückgesetzt. \n')

env = os.environ.get('APPNAME_ENV', 'dev')
app = create_app('app.config.%sConfig' % env.capitalize())

manager = Manager(app)

manager.add_command("show-urls", ShowUrls())
manager.add_command("clean", Clean())
manager.add_command("init-db", InitDatabase())

from flask_assets import ManageAssets
manager.add_command("assets", ManageAssets(assets_env))

@manager.shell
def make_shell_context():

    return dict(app=app)


if __name__ == "__main__":
    manager.run()
